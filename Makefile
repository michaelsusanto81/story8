PWD = $(shell pwd)
VENV_NAME = env
VENV_DIR = $(PWD)/$(VENV_NAME)
VENV_BIN = $(VENV_DIR)/bin
VENV_ACTIVATE = $(VENV_BIN)/activate

PYTHON = $(VENV_BIN)/python3
REQS = $(PWD)/requirements.txt
MANAGE = $(PWD)/manage.py

all: clean install prepare run

create_env:
	test -d $(VENV_DIR) || python3 -m venv $(VENV_DIR)

activate_env:
	. $(VENV_ACTIVATE)

install_env:
	$(PYTHON) -m pip install -r $(REQS)

collect:
	$(PYTHON) $(MANAGE) collectstatic --no-input

migrate:
	$(PYTHON) $(MANAGE) makemigrations
	$(PYTHON) $(MANAGE) migrate

clean:
	rm -f $(PWD)/*.pyc

install: create_env activate_env install_env

prepare: collect migrate

run:
	$(PYTHON) $(MANAGE) runserver
