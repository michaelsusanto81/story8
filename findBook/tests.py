from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.http import HttpRequest
from findBook.views import index
from findBook.forms import BookForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import presence_of_element_located

# Create your tests here.
class FindBookUnitTest(TestCase):

	# test if url is exist
	def test_home_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	# test if url is not exist
	def test_url_is_not_exist(self):
		response = Client().get('/wek/')
		self.assertEqual(response.status_code, 404)

	# test if home using index func
	def test_home_using_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	# test if home using index.html
	def test_home_using_index_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'index.html')

	# test if home landing page is completed
	def test_home_landing_page_is_completed(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('FindBook', html_response)

	# test if book form has placeholder
	def test_book_form_has_placeholder(self):
		form = BookForm()
		self.assertIn('id="id_searchBox', form.as_p())

	# test book form blank validation
	def test_book_form_validation_for_blank_items(self):
		form = BookForm(data={'searchBox':''})
		self.assertFalse(form.is_valid())
		self.assertEqual(form.errors['searchBox'], ["This field is required."])

class StatusFunctionalTest(LiveServerTestCase):

	# set up selenium
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--window-size=1420,1080')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium = webdriver.Chrome(chrome_options=chrome_options)

    # close selenium
    def tearDown(self):
        self.selenium.quit()
        super(StatusFunctionalTest, self).tearDown()

    # main flow of functional test
    def test_input_todo(self):

    	# open selenium
        selenium = self.selenium

        # open localhost
        selenium.get('http://127.0.0.1:8000/')

        # find the form element
        searchBox = selenium.find_element_by_id('id_searchBox')
        submit = selenium.find_element_by_id('submit')

        # fill form with data
        searchBox.send_keys('web')

        # submit form
        submit.send_keys(Keys.RETURN)

        wait = WebDriverWait(self.selenium, 10)
        wait.until(presence_of_element_located((By.TAG_NAME, 'tr')))
        self.assertIn('Web', self.selenium.page_source)