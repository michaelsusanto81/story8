from django.urls import path
from . import views

app_name = "findBook"

urlpatterns = [
    path('', views.index, name='index'),
]