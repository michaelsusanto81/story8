from django.shortcuts import render
from findBook.forms import BookForm
from django.http import JsonResponse
import json

# Create your views here.
def index(request):
	response = {}
	form = BookForm(request.POST or None)
	response['form'] = form
	return render(request, 'index.html', response)