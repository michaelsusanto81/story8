$(function() {
	$.ajax({
		method: 'GET',
		url: 'https://www.googleapis.com/books/v1/volumes?q=web',
		dataType: 'json',
		success: function(data) {
			// console.log(data);
			$('#searchResult').empty();
			var html = '<tr><th>Cover</th><th>Title</th><th>Author</th><th>Publisher</th><th>Published Date</th></tr>';
			for(var i = 0; i < data.items.length; i++) {
				html += '<tr><td><img src="' +
				data.items[i].volumeInfo.imageLinks.smallThumbnail + '"></td><td>' +
				data.items[i].volumeInfo.title + '</td><td>' +
				data.items[i].volumeInfo.authors + '</td><td>' +
				data.items[i].volumeInfo.publisher + '</td><td>' +
				data.items[i].volumeInfo.publishedDate + '</td></tr>';
			}
			$('#searchResult').append(html);
			$('#searchResult th').css({
				'font-size': '24px',
				'padding': '10px',
				'border': '3px solid #add6ff'
			});
			$('#searchResult td').css({
				'font-size': '18px',
				'padding': '10px',
				'border': '3px solid #add6ff'
			});
		}
	})
	
	$('#submit').click(function() {
		var keyword = $('#id_searchBox').val();
		$.ajax({
			method: 'GET',
			url: 'https://www.googleapis.com/books/v1/volumes?q=' + keyword,
			dataType: 'json',
			success: function(data) {
				// console.log(data);
				$('#searchResult').empty();
				var html = '<tr><th>Cover</th><th>Title</th><th>Author</th><th>Publisher</th><th>Published Date</th></tr>';
				for(var i = 0; i < data.items.length; i++) {
					html += '<tr><td><img src="' +
					data.items[i].volumeInfo.imageLinks.smallThumbnail + '"></td><td>' +
					data.items[i].volumeInfo.title + '</td><td>' +
					data.items[i].volumeInfo.authors + '</td><td>' +
					data.items[i].volumeInfo.publisher + '</td><td>' +
					data.items[i].volumeInfo.publishedDate + '</td></tr>';
				}
				$('#searchResult').append(html);
				$('#searchResult th').css({
					'font-size': '24px',
					'padding': '10px',
					'border': '3px solid #add6ff'
				});
				$('#searchResult td').css({
					'font-size': '18px',
					'padding': '10px',
					'border': '3px solid #add6ff'
				});
			}
		})
	})	
})